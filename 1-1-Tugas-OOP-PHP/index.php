<?PHP
    trait hewan{
        //nama, darah dengan nilai default 50, jumlahKaki, dan keahlian.
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi(){
            echo "$this->nama  sedang melakukan $this->keahlian";
            echo "<br>";
        }
    }
    abstract class fight{
        use hewan;
        public $attackPower;
        public $defensePower;

        public function menyerang($hewan){
            echo "$this->nama menyerang $hewan->nama, ";
            $hewan->diserang($this);
        }
        public function diserang($hewan){
            echo " $this->nama diserang $hewan->nama";
            //  $hewan->menyerang($this);
             $this->darah = $this->darah -($hewan->attackPower / $this->defensePower);
        }

        protected function getInfo(){
            echo "<br>";
            echo "nama : {$this->nama}";
            echo "<br>";
            echo "Jumlah kaki : {$this->jumlahKaki}";
            echo "<br>";
            echo "Keahlian : {$this->keahlian}";
            echo "<br>";
            echo "Darah : {$this->darah}";
            echo "<br>";
            echo "Attack power : {$this->attackPower}";
            echo "<br>";
            echo "Defense power : {$this->defensePower}";
            echo "<br>";
            $this->atraksi();
            // echo "<br>";
        }

        // abstract public function getInfo();
        abstract public function getInfoHewan();
    }
    class Pemisah{
        public static function show(){
            echo "<br>";
            echo "============================================";
            echo "<br>";
        }
    }
    class Harimau extends fight{

        public function __construct($nama){
            $this->nama = $nama;
            $this->jumlahKaki = 4;
            $this->keahlian = 'lari cepat';
            $this->attackPower = 7;
            $this->defensePower = 8;

        }
        public function getInfoHewan(){
            echo "Informasi hewan harimau";
            $this->getInfo();
        }
    }

    class Elang extends fight{

        public function __construct($nama){
            $this->nama = $nama;
            $this->jumlahKaki = 2;
            $this->keahlian = 'terbang tinggi';
            $this->attackPower = 10;
            $this->defensePower = 5;

        }
        public function getInfoHewan(){
            echo "Informasi hewan elang";
            $this->getInfo();
        }
    }
    $harimau = new Harimau('harimau');
    $harimau->getInfoHewan();
    pemisah::show();
    $elang = new Elang('elang');
    $elang->getInfoHewan();
    pemisah::show();
    $harimau->menyerang($elang);
    pemisah::show();
    // $harimau->getInfoHewan();
    pemisah::show();
    // $elang = new Elang('elang');
    $elang->getInfoHewan();
    pemisah::show();
    $elang->menyerang($harimau);
    pemisah::show();
    $harimau->getInfoHewan();
    pemisah::show();
    // $elang->getInfoHewan();
?>  