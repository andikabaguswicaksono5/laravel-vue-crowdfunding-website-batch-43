<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title')->unique();
            // description	text	 	 
            $table->text('description');
            // address	text	 
            $table->text('address');	 
            // image	uuid	nullable
            $table->string('image')->nullable();	 
            // required	integer	
            $table->integer('required'); 	 
            // collected	integer	
            $table->integer('collected'); 	 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
};
