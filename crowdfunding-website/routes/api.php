<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CampaignControler;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('register', [AuthController::class, 'Register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('generate-otp-code', [AuthController::class, 'generateOtp']);
    Route::post('verification-email', [AuthController::class, 'emailVerification']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware(['auth','email_verified','nonAdminRole']);
});

Route::get('get-profile', [AuthController::class, 'me'])->middleware(['auth','email_verified','nonAdminRole']);
Route::post('edit-password', [AuthController::class, 'editPassword'])->middleware(['auth','email_verified','nonAdminRole']);
Route::post('edit-profile', [AuthController::class, 'editProfile'])->middleware(['auth','email_verified','nonAdminRole']);

Route::apiResource('campaign',CampaignControler::class)->middleware(['auth','email_verified','isAdminRole']); //,'isAdminRole'