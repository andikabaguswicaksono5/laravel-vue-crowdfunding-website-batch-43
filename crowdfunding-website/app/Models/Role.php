<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Support\str;
use App\Traits\UsesUuid;

class Role extends Model
{
    use HasFactory, UsesUuid;
    protected $fillable = ['name'];
    protected $primaryKey = 'id';



    public function roles()
    {
        return $this->hasMany(user::class, 'role_id');
    }
}
