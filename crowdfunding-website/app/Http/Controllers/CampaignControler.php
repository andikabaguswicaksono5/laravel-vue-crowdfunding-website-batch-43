<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;
use File;
class CampaignControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['campaign'] = Campaign::All();
        if(count($data['campaign'])==0){
            return response()->json([
                'response code' => '01',
                'response message' => 'Tidak terdapat data campaign yang ditampilkan'
            ],200);
        }else{
            return response()->json([
                'response code' => '00',
                'response message' => 'Data campaign berhasil ditampilkan',
                'data' => $data,
                'jumlah data' => count($data['campaign'])
            ],200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'required|unique:campaigns,title',
            'image' => 'mimes:png,jgp,jpeg|max:2048',           
            'address'=> 'required',
            'collected' => 'required|numeric',
            'required' => 'required|numeric',
        ],
        [
            'title.required' => 'Title wajib diisi',
            'title.unique' => 'Title sudah tersimpan di databaase',

            'image.mimes' => 'Format gambar tidak sesuai, (png,jgp,jpeg)',
            'image.max' => 'Ukuran gambar melebihi ukuran maksimal yang diijinkan',

            'address.required' => 'Addrress wajib diisi',
            'collected.required' => 'Collected wajib diisi',
            'required.required' => 'Required wajib diisi',
        ]);

        $Campaign = new Campaign;
 
        $Campaign->title = $request->title;
        $Campaign->description = $request->description;
        $Campaign->address = $request->address;
        $Campaign->required = $request->required;
        $Campaign->collected = $request->collected;

        if ($request->hasFile('image')){
            $photo_campaign = $request->file('image');
            $photo_campaign_extention = $photo_campaign->getClientOriginalExtension();
            $photo_campaign_name=time().'.'.$photo_campaign_extention;
            $photo_campaign_folder = 'image/campaign/photo/';
            $photo_campaign_location = $photo_campaign_folder.$photo_campaign_name;

            try {
                //code...
                $photo_campaign->move(public_path($photo_campaign_folder), $photo_campaign_name);
                $Campaign->image  = $photo_campaign_location;
    
            } catch (\Throwable $th) {
                //throw $th;
                return response()->json([
                    'response_code' => 01,
                    'response_message' => 'Upload foto gagal',
                ],400);
            }
        }


        $Campaign->save();
        return response()->json([
            'response_code' => 00,
            'response_message' => 'Data campaign berhasil disimpan',
            'data' => $Campaign
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Campaign::find($id);


        return response()->json([
            'response_code' => 00,
            'response_message' => 'Detail data campaign',
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $request->validate([
            'title' => 'required|unique:campaigns,title',
            'image' => 'mimes:png,jgp,jpeg|max:2048',           
            'address'=> 'required',
            'collected' => 'required|numeric',
            'required' => 'required|numeric',
        ],
        [
            'title.required' => 'Title wajib diisi',
            'title.unique' => 'Title sudah tersimpan di databaase',

            'image.mimes' => 'Format gambar tidak sesuai, (png,jgp,jpeg)',
            'image.max' => 'Ukuran gambar melebihi ukuran maksimal yang diijinkan',

            'address.required' => 'Addrress wajib diisi',
            'collected.required' => 'Collected wajib diisi',
            'required.required' => 'Required wajib diisi',
        ]);

        $Campaign = Campaign::find($id);
 
        $Campaign->title = $request->title;
        $Campaign->description = $request->description;
        $Campaign->address = $request->address;
        $Campaign->required = $request->required;
        $Campaign->collected = $request->collected;

        if ($request->hasFile('image')){
            $photo_campaign = $request->file('image');
            $photo_campaign_extention = $photo_campaign->getClientOriginalExtension();
            $photo_campaign_name=time().'.'.$photo_campaign_extention;
            $photo_campaign_folder = 'image/campaign/photo/';

            $subCampaign = substr($Campaign->image,1);
            File::delete($subCampaign);


            $photo_campaign_location = $photo_campaign_folder.$photo_campaign_name;

            try {
                //code...
                $photo_campaign->move(public_path($photo_campaign_folder), $photo_campaign_name);
                $Campaign->image  = $photo_campaign_location;
    
            } catch (\Throwable $th) {
                //throw $th;
                return response()->json([
                    'response_code' => 01,
                    'response_message' => 'Upload foto gagal',
                ],400);
            }
        }


        $Campaign->save();
        return response()->json([
            'response_code' => 00,
            'response_message' => 'Update campaign berhasil',
            'data' => $Campaign
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Campaign = Campaign::find($id);
        $subCampaign = substr($Campaign->image,1);
        File::delete($subCampaign);
        $Campaign->delete();

        return response()->json([
            'response_code' => 00,
            'response_message' => 'Delete data campaign berhasil'
        ],200);
    }
}
