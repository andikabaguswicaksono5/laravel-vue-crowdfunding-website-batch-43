<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\otpCode;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use File;
use carbon\carbon;
use App\Events\UserRegisterEvent;

class AuthController extends Controller
{
    //
    public function Register(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'name' => 'required|max:255'
        ]);

        $user = user::Create([
            'email' => $request['email'],
            // 'password' => $request['password'],
            'password' => Hash::make($request->password),
            'name' => $request['name'],
        ]);
        $data['user'] = $user;
        

        event(new UserRegisterEvent($user));

        return response()->json([
            'response_code' => 00,
            'response_message' => 'user berhasil register',
            'data' => $user
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'email tidak terdaftar atau password yang dimasukan tidak sesuai'], 401);
        }

        $data['token'] = $token;
        return response()->json([
            'response_code' => 00,
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token
            ]
        ], 201);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Berhasil logout']);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function editPassword(Request $request)
    {
        $userId = auth::id();
        $request->validate([
            'password' => 'required|min:8|confirmed'
        ]);
        user::where('email', $request['email'])
            ->update([
                'password' => Hash::make($request->password),
            ]);

        return response()->json([
            'response_code' => 00,
            'response_message' => 'Password user berhasil di update',
        ], 201);
    }

    public function editProfile(Request $request)
    {
        $userId = auth::id();
        $photoProfileUpddate = '';
        $request->validate([
            'name' => 'max:255',
            'photo_profile' => 'mimes:png,jgp,jpeg|max:2048'
        ]);
        if ($request->has('photo_profile')){
            $photo_profile = $request->file('photo_profile');
            $photo_profile_extention = $photo_profile->getClientOriginalExtension();
            $photo_profile_name=time().'.'.$photo_profile_extention;

            $photo_profile_folder = 'image/user/photo_profile/';
            $photo_profile_location = $photo_profile_folder.$photo_profile_name;

            try {
                //code...
                $photo_profile->move(public_path($photo_profile_folder), $photo_profile_name);
                $photoProfileUpddate  = $photo_profile_location;
            } catch (\Throwable $th) {
                //throw $th;
                return response()->json([
                    'response_code' => 01,
                    'response_message' => 'Upload foto gagal',
                ],400);
            }

           
        }
        $user = user::where('id',$userId)
        -> update([
            'name' => $request['name'],
            'photo_profile' => $photoProfileUpddate,

        ]);
        $data['user'] = $user;
        return response()->json([
            'response_code' => 00,
            'response_message' => 'user berhasil di update',
            'data' => auth::user(),
            'something' => $userId
        ],201);
    }

    public function generateOtp(Request $request){
        $request->validate([
            'email' => 'email|required',
        ]);

        $user=User::where('email', $request->email)->first();
        $user->generate_otp_code();
        $data['user']=$user;
        //test kirim email
        Mail::to($user->email)->send(new GenerateOtpMail($user)); //<---- ini nanti dipindah ke listener generate otpcode
        return response()->json([
            'response_code' => 00,
            'response_message' => 'otp code berhasil di generate'
        ],200);
    }

    public function emailVerification(Request $request){
        $request->validate([
            'otp' => 'required',
        ]);
        $otp_code = otpCode::where('otp', $request->otp)->first();
        if(!$otp_code){
            return response()->json([
                'response_code' => 01,
                'response_message' => 'otp code tidak ditemukan'
            ],400);
        }
        $now = carbon::now();
        if($now > $otp_code->valid_until){
            return response()->json([
                "response_code"=> "01",
                "response_message"=> "otp code sudah tidak berlaku, silahkan generate ulang"
            ],400);
        }
        //update user
        $user=User::find( $otp_code->user_id);
        $user->email_verified_at =$now ;
        $user->save();

        //delete otpcode
        $otp_code->delete();
        return response()->json([
            "response_code"=> "00",
            "response_message"=> "email sudah terverifikasi"
        ],200);
    }
}
