<?php

namespace App\Traits;

use Illuminate\Support\str;

trait UsesUuid
{

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected static function BootUsesUuid()
    {

        static::creating(function ($model) {

            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = str::uuid();
            }
        });
    }
}
